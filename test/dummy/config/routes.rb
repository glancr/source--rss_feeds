# frozen_string_literal: true

Rails.application.routes.draw do
  mount RssFeeds::Engine => '/rss_feeds'
end
