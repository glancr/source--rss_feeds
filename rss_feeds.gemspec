# frozen_string_literal: true

require 'json'

$LOAD_PATH.push File.expand_path('lib', __dir__)

# Maintain your gem's version:
require 'rss_feeds/version'

# Describe your gem and declare its dependencies:
Gem::Specification.new do |s|
  s.name        = 'rss_feeds'
  s.version     = RssFeeds::VERSION
  s.authors     = ['Tobias Grasse']
  s.email       = ['tg@glancr.de']
  s.homepage    = 'https://glancr.de/modules/rss_feeds'
  s.summary     = 'Summary of RssFeeds.'
  s.description = 'Fetch the five latest items from RSS/Atom newsfeeds.'
  s.license     = 'MIT'
  s.metadata    = { 'json' =>
                {
                  type: 'sources',
                  title: {
                    enGb: 'RSS Feeds',
                    deDe: 'RSS-Feeds',
                    frFr: 'Flux RSS',
                    esEs: 'Canales RSS',
                    plPl: 'Kanały RSS',
                    koKr: 'RSS 피드'
                  },
                  description: {
                    enGb: s.description,
                    deDe: 'Lade die fünf neuesten Einträge aus RSS/Atom Newsfeeds.',
                    frFr: 'Téléchargez les cinq dernières entrées des flux de nouvelles RSS/Atom.',
                    esEs: 'Descargue las cinco últimas entradas de los canales de noticias RSS/Atom.',
                    plPl: 'Pobierz pięć najnowszych wpisów z kanałów informacyjnych RSS/Atom.',
                    koKr: 'RSS / Atom 뉴스 피드에서 5 개의 최신 항목을로드하십시오.'
                  },
                  groups: ['newsfeed'],
                  compatibility: '0.10.4'
                }.to_json }

  s.files = Dir['{app,config,db,lib}/**/*', 'MIT-LICENSE', 'Rakefile', 'README.md']

  s.add_dependency 'feedjira', '~> 3.2'
  s.add_dependency 'httparty', '~> 0.20'
  s.add_development_dependency 'rails', '~> 5.2'
  s.add_development_dependency 'rubocop'
  s.add_development_dependency 'rubocop-rails'
end
