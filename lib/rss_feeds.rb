# frozen_string_literal: true

require 'rss_feeds/engine'
require 'rss_feeds/extended_rss_parser'
require 'rss_feeds/extended_atom_parser'
require 'httparty'
require 'feedjira'

module RssFeeds
  class Hooks
    REFRESH_INTERVAL = '15m'
    FETCH_RANGE = (0..9).freeze

    # Headers are capitalized, thus need to be strings.
    # noinspection RubyStringKeysInHashInspection
    REQ_HEADERS = {
      'Accept' => 'application/rss+xml, application/rdf+xml;q=0.8, application/atom+xml;q=0.6, application/xml;q=0.4, text/xml;q=0.4'
    }.freeze

    # @return [String]
    def self.refresh_interval
      REFRESH_INTERVAL
    end

    # @param [Hash] configuration
    def initialize(instance_id, configuration)
      @instance_id = instance_id
      @url = configuration['feedUrl']
    end

    def default_title
      newsfeed = fetch_and_parse @url
      newsfeed.title || "Feed #{@url.slice(0, 10)}..."
    end

    def validate_configuration
      # TODO: Add credential validation if present.
      fetch_and_parse @url
    end

    def list_sub_resources
      newsfeed = fetch_and_parse(@url)
      [[@url, newsfeed.title]]
    end

    def fetch_data(_group, sub_resources)
      records = []
      sub_resources.each do |url|
        newsfeed = fetch_and_parse(url)
        records << map_feed(newsfeed)
      end
      records
    end

    private

    def fetch_and_parse(url)
      res = HTTParty.get(url, headers: REQ_HEADERS)

      Feedjira.configure do |config|
        config.parsers.unshift(ExtendedRSSParser, ExtendedAtomParser)
      end
      Feedjira.parse(res.body)
    end

    # TODO: A lot of code is duplicated here. Only differs in the parsed feed's structure.

    def map_feed(newsfeed)
      feed = Newsfeed.find_or_initialize_by(id: @url) do |nf|
        nf.id = @url
      end

      logo_url = newsfeed.image&.instance_of?(Feedjira::Parser::RSSImage) ? newsfeed.image.url : newsfeed.image
      feed.assign_attributes(
        url: @url,
        name: newsfeed.title,
        icon_url: logo_url,
        latest_entry: newsfeed.last_built
      )

      current_uids = []
      newsfeed.entries[FETCH_RANGE].each do |entry|
        # Some feeds have GUIDs with >> 255 chars, use hexdigest for those
        uid = if entry.entry_id && entry.entry_id.length <= 255
                entry.entry_id
              else
                Digest::MD5.hexdigest(entry.url)
              end
        current_uids << uid
        attributes = {
          title: entry.title,
          content: entry.summary,
          url: entry.url,
          published: entry.published
        }
        feed.update_or_insert_child(uid, attributes)
      end

      feed.items.each do |item|
        item.mark_for_destruction unless current_uids.include?(item.uid)
      end

      feed
    end
  end
end
