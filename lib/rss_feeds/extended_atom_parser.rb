# frozen_string_literal: true

require 'feedjira'

module RssFeeds
  # Extends the Feedjira Atom parser to provide the logo URL, and a unified
  # feed update timestamp API.
  class ExtendedAtomParser < Feedjira::Parser::Atom
    element :logo, as: :image
    element :updated, as: :last_built
  end
end
