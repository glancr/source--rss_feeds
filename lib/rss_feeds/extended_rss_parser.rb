# frozen_string_literal: true

require 'feedjira'

module RssFeeds
  # Parser for dealing with RSS feeds.
  class ExtendedRSSParser < Feedjira::Parser::RSS
    # DublinCore extensions, currently only to map the channel's date attribute.
    # All other items used in GroupSchemas::Newsfeed are required in RSS 1.0 and
    # thus available without DC mappings.
    # @see https://github.com/feedjira/feedjira/blob/v3.1.0/lib/feedjira/rss_entry_utilities.rb
    # for mapping logic.
    element :'dc:date', as: :last_built
  end
end
