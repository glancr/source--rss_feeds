# frozen_string_literal: true

module RssFeeds
  class ApplicationRecord < ActiveRecord::Base
    self.abstract_class = true
  end
end
